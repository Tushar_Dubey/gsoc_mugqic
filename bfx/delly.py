# Python Standard Modules
import os

# MUGQIC Modules
from core.config import *
from core.job import *

def Delly(SV,query,output,ref,other_options):
	return Job(
        [query],
        [output],
        command="""\
 Delly2 -t {SV} -o {output}{ref} \\ 
  {query}{other_options}""".format(
        ref=" \\\n  -g " + ref if ref else "",
        SV=SV,
        query=query,
        other_options=" \\\n  -x " + other_options if other_options else "",
        output=output
        )
    )