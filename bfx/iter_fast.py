def iter_fasta(path):
    with open(path) as handle:
        header = None
        buf = []
        for line in handle:
            if line.isspace():
                continue  # Omit empty lines.

            line = line.strip()
            if line.startswith(">"):
                if buf:
                    assert header is not None
                    yield header, "".join(buf)
                    del buf[:]

                header = line[1:]  # Drop the '>'.
            else:
                buf.append(line)

        # Handle the last record.
        if buf:
            yield header, "".join(buf)